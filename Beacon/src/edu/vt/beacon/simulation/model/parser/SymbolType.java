package edu.vt.beacon.simulation.model.parser;

public enum SymbolType {
	
	ST_OPENBRACKET,
	ST_CLOSEBRACKET,
	ST_NEGATION,
	ST_OPERATOR,
	ST_CONSTANT,
    ST_OPENTEMP,
    ST_CLOSETEMP,
    ST_MINUS,
    ST_MATH,
    ST_NUMBER,
    ST_VAR,
    ST_DASH,
    ST_COMMA

}
