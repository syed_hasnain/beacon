package edu.vt.beacon.simulation.interfaces;

public interface NetworkListener {
	
	public void applyLiteralDecision(int decision);

}
