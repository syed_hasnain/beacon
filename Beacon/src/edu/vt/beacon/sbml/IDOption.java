package edu.vt.beacon.sbml;

/**
 * Created by marakeby on 4/4/17.
 */
public enum IDOption {
    Id, Name;
}
